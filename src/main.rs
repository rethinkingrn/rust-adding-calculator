use std::i32;
use std::io;

fn main() {
    println!("Addition program");

    // Variables
    let mut x = String::new();
    let mut y = String::new();

    // Grabs user input
    println!("Enter the first number");
    io::stdin()
        .read_line(&mut x)
        .ok()
        .expect("Failed to read line");

    println!("Enter the second number");
    io::stdin()
        .read_line(&mut y)
        .ok()
        .expect("Failed to read line");
    // Converting string to integer
    let xint: i32 = x
        .trim()
        .parse()
        .ok()
        .expect("Program only processes numbers, Enter number");
    let yint: i32 = y
        .trim()
        .parse()
        .ok()
        .expect("Program only processes numbers, Enter number");

    // Prints total
    println!("Total is: {}", xint + yint);
}
